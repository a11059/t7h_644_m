A slightly modified version and configuration of the original m firmware for avr transtor testers by Markus Reschke. 
Available at  https://github.com/kubi48/TransistorTester-source.git
it works on the Chinese T7H-LCR with atmega644 mcu, probably also 344 if a couple of options switched off , pictured below.
Adds a power config option ATAR_TC1_U4 this changes how the power pin is driven allowing a SCT15L104W with https://github.com/atar-axis/tc1-u4 to work properly.
By default when in high state it is driven to 5v which stops the button output from the STC working for some reason. 
It will not work without the tc1-u4 firmware, I fitted a new one so could keep old for backup these mcus are not readable for backup.

also see https://gitlab.com/a11059/tc1-u4/-/tree/sdcc/tc1-u4 for code that will compile under SDCC instead of Keil. 

The configuration is for bitbang SPI, weird choice of pins. See pictures for the unit it has been tested on below.  
The repository also includes a circuit diagram which I think is reasonably accurate but may have errors.  
For further info and instructions see the original readme included in the repo.

![T7H-LCR with Atmega328](/photos/IMG_3687.JPG "Case")
![T7H-LCR with Atmega328](/photos/IMG_3688.JPG "PCB Back")
![T7H-LCR with Atmega328](/photos/IMG_3689.JPG "PCB Front")

More images in photos folder.
